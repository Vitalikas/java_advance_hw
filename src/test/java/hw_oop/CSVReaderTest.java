package hw_oop;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVReaderTest {
    Object[] expectedItems = {new Item("Stalas", 10.99, 55, 21),
            new Item("Kėdė", 3.99, 200, 15),
            new Item("Lova", 83.99, 14, 16),
            new Item("Spinta", 280.19, 2, 21),
            new Item("Lentyna", 12.62, 40, 18),
            new Item("Komoda", 27.15, 12, 11),
            new Item("Pagalvė", 8.99, 47, 17),
            new Item("Antklodė", 3.99, 148, 13),
            new Item("Patalynė", 87.44, 23, 14),
            new Item("Kilimas", 11.27, 88, 20)
    };

    @Before
    public void setUp() {
    }

    @Test
    public void compareItemLists() {
        String CSV_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\data.csv";
        Object[] result = new CSVReader().readCSV(CSV_FILE_PATH).toArray();
        assertArrayEquals(expectedItems, result);
    }
}