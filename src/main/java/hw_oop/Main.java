package hw_oop;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        final String CSV_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\data.csv";

        CSVReader reader = new CSVReader();

        // nuskaitome CSV failą
        List<Item> items = reader.readCSV(CSV_FILE_PATH);

        reader.printItems(items);

        Map<Double, List<Item>> itemsMap = items.stream()
                .collect(Collectors.groupingBy(Item::getPrice));

        // with formatting
        System.out.println(mapToString(itemsMap));

        // apskaičiuojame prekių kainas su PVM, bendrą prekių kiekį ir sumą
        AtomicInteger totalQuantity = new AtomicInteger();
        AtomicReference<Double> totalPrice = new AtomicReference<>(0.0);

        items.forEach(item -> {
            double priceWithVAT = item.getPrice() * (item.getVat() + 100) / 100;

            totalQuantity.addAndGet(item.getQuantity());

            totalPrice.updateAndGet(price -> price + item.getQuantity() * priceWithVAT);
        });

        //Comparable<>, Comparator<> interfeisų implementacijos
        List<Object> formats = new ArrayList<>();

        formats.add("========================================================");
        formats.add("%-15s %-10s %-10s %-10s %7s");
        formats.add("--------------------------------------------------------");
        formats.add("%-15s %-10.2f %-10d %-10d %7.2f");
        formats.add("%s %25d %25.2f");

        System.out.println("\nAvailable sorting methods:");

        SortingMethod[] sortingMethods = SortingMethod.values();
        for (SortingMethod method : sortingMethods) {
            System.out.println(method);
        }
        System.out.println();
        System.out.print("Please enter sorting method: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        switch (SortingMethod.valueOf(input)) {
            case compareTo:
                Collections.sort(items);

                System.out.println("Sorting by price and name (if price the same) in ascending order " +
                        "implementing Comparable<> interface with compareTo() method");
                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                        );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                String answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("Sorting by price and name (if price the same) in ascending order " +
                                    "implementing Comparable<> interface with compareTo() method");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

                break;
            case comparator:
                items.sort(new ItemDescendingComparator());

                System.out.println("Sorting by price and quantity (if price the same) in descending order " +
                        "implementing Comparator<> interface with compare() method");
                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("Sorting by price and quantity (if price the same) in descending order " +
                                    "implementing Comparator<> interface with compare() method");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

                break;
            case anonymousComparator:
                Collections.sort(items, new Comparator<Item>() {
                    @Override
                    public int compare(Item item1, Item item2) {
                        if (item1.getPrice() == item2.getPrice()) {
                            return Integer.compare(item2.getQuantity(), item1.getQuantity());
                            // return item2.getQuantity() - item1.getQuantity();
                        } else {
                            return Double.compare(item2.getPrice(), item1.getPrice());
                            // return (int) (item2.getPrice() - item1.getPrice());
                        }
                    }
                });

                System.out.println("Sorting by price and quantity (if price the same) in descending order implementing " +
                        "anonymous Comparator<> interface with compare() method");
                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("Sorting by price and quantity (if price the same) in descending " +
                                    "order implementing anonymous Comparator<> interface with compare() method");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

                break;
            case lambda:
                items.sort((item1, item2) -> {
                    if (item1.getPrice() == item2.getPrice()) {
                        return Integer.compare(item2.getQuantity(), item1.getQuantity());
                        // return item2.getQuantity() - item1.getQuantity();
                    } else {
                        return Double.compare(item2.getPrice(), item1.getPrice());
                        // return (int) (item2.getPrice() - item1.getPrice());
                    }
                });

                System.out.println("Sorting by price and quantity (if price the same) in descending order with " +
                        "lambda expressions");
                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("Sorting by price and quantity (if price the same) in descending " +
                                    "order with lambda expressions");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

                break;
            case reverse:
                Collections.reverse(items);
                System.out.println("With reverse() sorting method");

                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("With reverse() sorting method");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

                break;
            case none:
                System.out.println("Without sorting method");
                System.out.println(formats.get(0));
                System.out.printf(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM,
                        Field.SUMA);
                System.out.println();
                System.out.println(formats.get(2));
                items.forEach(item -> {
                            System.out.printf(formats.get(3).toString(),
                                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                                            item.getQuantity()) * 100.00) / 100.00);
                            System.out.println();
                        }
                );
                System.out.println(formats.get(2));
                System.out.printf(formats.get(4).toString(), "Suma", totalQuantity.get(),
                        Math.round(totalPrice.get() * 100.0) / 100.0);
                System.out.println();
                System.out.println(formats.get(0));

                System.out.print("Print results to file? ");
                answer = scanner.nextLine();
                switch (answer) {
                    case "YES":
                        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write("Without sorting method");
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(1).toString(), Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM, Field.SUMA));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        items.forEach(item -> writeToFile(item));

                        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
                            bufferedWriter.write(formats.get(2).toString());
                            bufferedWriter.write("\n");
                            bufferedWriter.write(String.format(formats.get(4).toString(), "Suma", totalQuantity.get(),
                                    Math.round(totalPrice.get() * 100.0) / 100.0));
                            bufferedWriter.write("\n");
                            bufferedWriter.write(formats.get(0).toString());
                            bufferedWriter.write("\n\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Printed successfully");
                        break;
                    case "NO":
                        break;
                }

        }
    }

    public static <K, V> String mapToString(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .map(entry -> entry.getKey() + " -> " + entry.getValue())
                .collect(Collectors.joining(", ", "", ""));
    }

    public static void writeToFile(Item item) {
        final String OUTPUT_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\output.txt";
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_FILE_PATH, true))) {
            bufferedWriter.write(String.format("%-15s %-10.2f %-10d %-10d %7.2f\n",
                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat(),
                    Math.round((item.getPrice() * (item.getVat() + 100) / 100 *
                            item.getQuantity()) * 100.00) / 100.00));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}