package hw_oop;

public enum SortingMethod {
    compareTo,
    comparator,
    anonymousComparator,
    lambda,
    reverse,
    none
}