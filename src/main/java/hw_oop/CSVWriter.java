package hw_oop;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter {
    private static final String CSV_FILE_PATH = "C:\\Users\\Vi\\Desktop\\Java_advance_hw\\data.csv";

    public static void main(String[] args) {
        try(
            // BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(CSV_FILE_PATH));
            // BufferedWriter writer = Files.newBufferedWriter(Path.of(CSV_FILE_PATH));
            CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(new FileWriter(CSV_FILE_PATH)), CSVFormat.DEFAULT
                    .withHeader(String.valueOf(Field.NUMERIS), String.valueOf(Field.PAVADINIMAS),
                            String.valueOf(Field.KAINA), String.valueOf(Field.KIEKIS), String.valueOf(Field.PVM)))
            ) {
                csvPrinter.printRecord("1", "Stalas", "10.99", "55", "21");
                csvPrinter.printRecord("2", "Kėdė", "3.99", "200", "15");
                csvPrinter.printRecord("3", "Lova", "83.99", "14", "16");
                csvPrinter.printRecord("4", "Spinta", "280.19", "2", "21");
                csvPrinter.printRecord("5", "Lentyna", "12.62", "40", "18");
                csvPrinter.printRecord("6", "Komoda", "27.15", "12", "11");
                csvPrinter.printRecord("7", "Pagalvė", "8.99", "47", "17");
                csvPrinter.printRecord("8", "Antklodė", "3.99", "148", "13");
                csvPrinter.printRecord("9", "Patalynė", "87.44", "23", "14");
                csvPrinter.printRecord("10", "Kilimas", "11.27", "88", "20");
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}