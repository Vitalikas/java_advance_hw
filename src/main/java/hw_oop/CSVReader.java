package hw_oop;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    public List<Item> readCSV(String CSV_FILE_PATH) {
        List<Item> items = new ArrayList<>();
        try(
                // Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                // BufferedReader bufferedReader = new BufferedReader(new FileReader(CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(new BufferedReader(new FileReader(CSV_FILE_PATH)),
                    CSVFormat.DEFAULT.withFirstRecordAsHeader())
        ) {
            // csvRecords = csvParser.getRecords();
            csvParser.forEach(csvRecord -> items.add(new Item(csvRecord.get(1), Double.parseDouble(csvRecord.get(2)),
                    Integer.parseInt(csvRecord.get(3)), Integer.parseInt(csvRecord.get(4)))));
        } catch (IOException e) {
            e.printStackTrace();
        } return items;
    }

    public void printItems(List<Item> items) {
        System.out.println("Reading from CSV file... \n");
        System.out.printf("%-15s %-10s %-10s %s", Field.PAVADINIMAS, Field.KAINA, Field.KIEKIS, Field.PVM);
        System.out.println();
        System.out.println("-----------------------------------------");
        items.forEach(item -> {
            System.out.printf("%-15s %-10.2f %-10d %d",
                    item.getName(), item.getPrice(), item.getQuantity(), item.getVat());
            System.out.println();
        });
        System.out.println();
    }
}