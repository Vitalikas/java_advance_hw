package hw_oop;

import java.util.Comparator;

public class ItemDescendingComparator implements Comparator<Item> {

    @Override
    public int compare(Item item1, Item item2) {
//        if (item1.getPrice() < item2.getPrice()) {
//            return 1;
//        } else if (item1.getPrice() == item2.getPrice()) {
//            if (item1.getQuantity() < item2.getQuantity()) {
//                return 1;
//            } else if (item1.getQuantity() == item2.getQuantity()) {
//                return 0;
//            } else return -1;
//        } else {
//            return -1;
//        }

        if (item1.getPrice() == item2.getPrice()) {
            return Integer.compare(item2.getQuantity(), item1.getQuantity());
            // return item2.getQuantity() - item1.getQuantity();
        } else {
            return Double.compare(item2.getPrice(), item1.getPrice());
            // return (int) (item2.getPrice() - item1.getPrice());
        }
    }
}