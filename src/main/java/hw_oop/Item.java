package hw_oop;

import java.util.Objects;

public class Item implements Comparable<Item> {
    private String name;
    private double price;
    private int quantity;
    private int vat;

    public Item(String name, double price, int quantity, int vat) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.vat = vat;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getVat() {
        return vat;
    }

    @Override
    public int compareTo(Item other) {
//        if (this.price > other.price) {
//            return 1;
//        } else if (this.price == other.price) {
//            if (this.name.charAt(0) > other.name.charAt(0)) {
//                return 1;
//            } else if (this.name.charAt(0) == other.name.charAt(0)) {
//                return 0;
//            } else {
//                return -1;
//            }
//        } else {
//            return -1;
//        }
        if (this.price == other.price) {
            return this.name.compareToIgnoreCase(other.name);
        } else {
            return (int) (this.price - other.price);
            //return Double.compare(this.price, other.price);
        }
    }

    @Override
    public String toString() {
        return String.format("%s %.2f %d %d" , this.name, this.price, this.quantity, this.vat);
    }

    @Override
    public boolean equals(Object o) {
        Item item = (Item) o;
        boolean status = false;
        if (this.name.equalsIgnoreCase(item.name) && this.price == item.getPrice() &&
                this.quantity == item.getQuantity() && this.vat == item.vat) {
            status = true;
        } return status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, quantity, vat);
    }
}