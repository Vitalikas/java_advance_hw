package rob;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Item> items = new ArrayList<>();
        String line = "";
        String name = "";
        String price = "";
        String quantity = "";
        String tax = "";

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Vi\\Desktop\\data.csv"))) {
            while ((line = bufferedReader.readLine()) != null) {
                String[] strings = line.split(",");
                name = strings[0];
                price = strings[1];
                quantity = strings[2];
                tax = strings[3];

                items.add(new Item(name, Double.parseDouble(price), Integer.parseInt(quantity), Integer.parseInt(tax)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        items.forEach(System.out::println);

        Collections.sort(items);
        items.forEach(System.out::println);
    }
}