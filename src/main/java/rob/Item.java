package rob;

public class Item implements Comparable<Item> {
    String name;
    double price;
    int quantity;
    int tax;

    public Item(String name, double price, int quantity, int tax) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.tax = tax;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getTax() {
        return tax;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", tax=" + tax +
                '}';
    }


    @Override
    public int compareTo(Item other) {
        return (int) (this.price - other.price);
    }
}